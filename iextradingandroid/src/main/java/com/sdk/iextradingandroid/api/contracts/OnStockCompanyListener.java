package com.sdk.iextradingandroid.api.contracts;

import android.support.annotation.NonNull;
import com.sdk.iextradingandroid.business.StockCompany;

public interface OnStockCompanyListener extends OnIEXServiceErrorListener {
    void onCompanyInfo(@NonNull StockCompany stockCompany);
}
