package com.sdk.iextradingandroid.api;

import com.sdk.iextradingandroid.business.StockCompany;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface IEXService {
    @GET
    Observable<StockCompany> getCompanyInfo(@Url String url);

    @GET
    Observable<ResponseBody> getStockPrice(@Url String url);
}
