package com.sdk.iextradingandroid.api.contracts;

import android.support.annotation.NonNull;

import com.sdk.iextradingandroid.business.IEXServiceError;

public interface OnIEXServiceErrorListener {
    void onError(@NonNull IEXServiceError iexServiceError);
}
