package com.sdk.iextradingandroid.core;

import android.text.TextUtils;
import android.util.Log;
import com.sdk.iextradingandroid.api.contracts.OnStockCompanyListener;

public final class IEXTradingSdk {

    private static IEXTradingSdk iexTradingSdk;
    private IEXServiceManager iexServiceManager;


    private IEXTradingSdk() {
        iexServiceManager = IEXServiceManager.manage();
    }

    public static IEXTradingSdk initialize() {
        if (iexTradingSdk == null)
            iexTradingSdk = new IEXTradingSdk();
        return iexTradingSdk;
    }

    public void getStockCompany(String stockSymbol, OnStockCompanyListener onStockCompanyListener) {
        if ((TextUtils.isEmpty(stockSymbol) || stockSymbol.trim().length() == 0) || onStockCompanyListener == null)
            Log.e(getClass().getSimpleName(), "Either the stock listener or provided stock symbol can't be blank/null.");
        else if (stockSymbol.trim().length() > 0)
            iexServiceManager.getStockCompany(stockSymbol, onStockCompanyListener);
    }


}
