package com.sdk.iextradingandroid.core;

import android.util.Log;
import com.sdk.iextradingandroid.BuildConfig;
import com.sdk.iextradingandroid.api.IEXService;
import com.sdk.iextradingandroid.api.contracts.OnStockCompanyListener;
import com.sdk.iextradingandroid.business.IEXServiceError;
import com.sdk.iextradingandroid.business.StockCompany;
import com.sdk.iextradingandroid.business.StockPrice;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

final class IEXServiceManager {
    private static IEXServiceManager iexServiceManager;
    private Retrofit retrofit;
    private IEXService iexService;
    private IEXServiceError iexServiceError = new IEXServiceError();


    private IEXServiceManager() {
        initializeManager();
    }

    static IEXServiceManager manage() {
        if (iexServiceManager == null) {
            iexServiceManager = new IEXServiceManager();
        }
        return iexServiceManager;

    }

    private void initializeManager() {
        if (retrofit == null) {
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor()).build();
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(BuildConfig.BASE_URL)
                    .client(client)
                    .build();
            iexService = retrofit.create(IEXService.class);
        }


    }

    void getStockCompany(final String stockSymbol, final OnStockCompanyListener onStockCompanyListener) {
        String companyUrl = "stock/" + stockSymbol + "/company";
        String priceUrl = "stock/" + stockSymbol + "/price";
        Observable.zip(iexService.getCompanyInfo(companyUrl), iexService.getStockPrice(priceUrl), new BiFunction<StockCompany, ResponseBody, StockCompany>() {
            @Override
            public StockCompany apply(StockCompany stockCompany, ResponseBody responseBody) {
                try {
                    stockCompany.setStockPrice(new StockPrice(responseBody.string()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return stockCompany;
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<StockCompany>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(StockCompany stockCompany) {
                onStockCompanyListener.onCompanyInfo(stockCompany);
            }

            @Override
            public void onError(Throwable e) {
                onStockCompanyListener.onError(iexServiceError.setMessage(e.getMessage()));
            }

            @Override
            public void onComplete() {
                Log.i(getClass().getSimpleName(), "Stock Company Call Success");
            }
        });

    }

}
