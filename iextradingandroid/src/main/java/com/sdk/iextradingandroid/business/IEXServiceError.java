package com.sdk.iextradingandroid.business;

public final class IEXServiceError {
    private String message;

    public IEXServiceError(String message) {
        this.message = message;
    }

    public IEXServiceError() {
    }

    public String getMessage() {
        return message;
    }

    public IEXServiceError setMessage(String message) {
        this.message = message;
        return this;
    }
}
