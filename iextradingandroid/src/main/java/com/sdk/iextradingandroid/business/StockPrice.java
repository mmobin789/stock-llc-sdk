package com.sdk.iextradingandroid.business;

import android.support.annotation.NonNull;

public final class StockPrice extends BaseModel {
    private String price;

    public StockPrice(String price) {
        this.price = price;
    }

    @NonNull
    @Override
    public String toString() {
        return price;
    }
}
