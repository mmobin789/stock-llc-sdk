package com.sdk.iextradingandroid.business;

public final class StockCompany extends BaseModel {

    private String website;
    private String companyName;
    private String symbol;
    private String CEO;
    private StockPrice stockPrice;

    public String getWebsite() {
        return website;
    }


    public String getCompanyName() {
        return companyName;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getCEO() {
        return CEO;
    }

    public StockPrice getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(StockPrice stockPrice) {
        this.stockPrice = stockPrice;
    }
}
