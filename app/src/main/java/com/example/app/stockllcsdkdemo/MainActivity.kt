package com.example.app.stockllcsdkdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.sdk.iextradingandroid.api.contracts.OnStockCompanyListener
import com.sdk.iextradingandroid.business.IEXServiceError
import com.sdk.iextradingandroid.business.StockCompany
import com.sdk.iextradingandroid.core.IEXTradingSdk
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val iexTradingSdk = IEXTradingSdk.initialize()
        button.setOnClickListener {
            val symbol = editText.text.toString()
            if (!symbol.isBlank()) {
                iexTradingSdk.getStockCompany(symbol, object : OnStockCompanyListener {
                    override fun onCompanyInfo(stockCompany: StockCompany) {
                        val data =
                            "${stockCompany.companyName} ${stockCompany.website} ${stockCompany.ceo} ${stockCompany.stockPrice}"
                        textView.text = data
                    }

                    override fun onError(iexServiceError: IEXServiceError) {
                        val error = "${getString(R.string.no_company_found)} $symbol"
                        textView.text = error
                    }
                })
            }
        }
    }
}
